package org.ronruby.kafka

import java.util.{Date, Properties}
import org.apache.kafka.clients.producer.{KafkaProducer, ProducerRecord}
import scala.util.Random


object Producer {
  def main(args: Array[String]) {
    val events = 1000 // args(0).toInt
    val topic = "test_topic"// args(1)
    val brokers = ":::9092" // args(2)
    val props = new Properties()

    props.put("bootstrap.servers", brokers)
    //props.put("client.id", "ScalaProducerExample")
    props.put("key.serializer", "org.apache.kafka.common.serialization.StringSerializer")
    props.put("value.serializer", "org.apache.kafka.common.serialization.StringSerializer")

    val producer = new KafkaProducer[String, String](props)

    for (nEvents <- (0 to events)) {
      val runtime = new Date().getTime()
      val msg = runtime + "," + nEvents + ", " + brokers

      val data = new ProducerRecord(topic, brokers, msg)

      producer.send(data)

      println(s"Sent Message: $data")
    }

    producer.close()
  }
}